Contains all the instructions for how the loading script updates the local machine.  The active script should be located in home_config/ and named localinfo.txt.  localinto.txt should also be in the .gitignore and therefore the update script should read the specified machine type and update the proper file in load_info/ so that git can upload it.  The script should also check for updates to the specified info file before updating any other files.

File format:
  Machine: must match one of the json files in the load_info/ without the extension (ex: macOS)
  Files: list of dicts [
    1: {
      file: path to file from home_config
      target: path to file on machine from home
    }

    ...

  ]
