# Install Basics
sudo apt-get update -y
sudo apt-get install vim -y
sudo apt-get install zsh -y
# Change Shell
sudo chsh $USER -s $(which zsh)

# Setup config files
cp ~/home_config/linux/bashrc ~/.bashrc
cp ~/home_config/linux/zshrc ~/.zshrc
cp ~/home_config/all/vimrc ~/.vimrc
cp ~/home_config/all/gitconfig ~/.gitconfig

# Install Conda
sudo apt-get install bzip2 libxml2-dev -y
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod +x Miniconda3-latest-Linux-x86_64.sh
./Miniconda3-latest-Linux-x86_64.sh -b
rm Miniconda3-latest-Linux-x86_64.sh
. ~/.bashrc
