sudo apt-get update
sudo apt-get install vim
cp ~/home_config/linux/bashrc ~/.bashrc
cp ~/home_config/linux/zshrc ~/.zshrc
cp ~/home_config/all/vimrc ~/.vimrc
cp ~/home_config/all/gitconfig ~/.gitconfig
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
rm Miniconda3-latest-Linux-x86_64.sh
sudo apt-get update
. ~/.bashrc
